#!/bin/bash

BUSTYPE='--session'
BUSNAME="deepsea.neuro.system"
OBJECTNAME="/deepsea/neuro/system/hostname"
INTERFACENAME='deepsea.neuro.system.basic'




# Request name
METHOD='getHostName'
dbus-send $BUSTYPE --print-reply --dest=$BUSNAME $OBJECTNAME $INTERFACENAME.$METHOD
echo ""

# Set New name
[ -n "$1" ] &&  NEW_HOSTNAME=$1 || NEW_HOSTNAME='MORDOR'
METHOD='setHostName'
ARGUMENTS=string:$NEW_HOSTNAME
dbus-send $BUSTYPE --print-reply --dest=$BUSNAME $OBJECTNAME $INTERFACENAME.$METHOD $ARGUMENTS
echo ""

# Request name to see if new name is registered correctly
METHOD='getHostName'
dbus-send $BUSTYPE --print-reply --dest=$BUSNAME $OBJECTNAME $INTERFACENAME.$METHOD
echo ""


read -p "Kill the remote service Y/n ?" SEL
if [ $SEL == 'Y' ]; then

    METHOD='quit'
    dbus-send $BUSTYPE --print-reply --dest=$BUSNAME $OBJECTNAME $INTERFACENAME.$METHOD

fi

