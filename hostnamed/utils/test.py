#!/usr/bin/env python

import sys
from pydbus import SessionBus


BUSNAME = "deepsea.neuro.system"
OBJECTPATH = "/deepsea/neuro/system/hostname"


def main():
    try:
        NEWNAME = sys.argv[1]
    except Exception:
        NEWNAME = 'GONDOR'

    bus = SessionBus()
    hostname_obj = bus.get(BUSNAME, OBJECTPATH)

    # Get hostame
    current_name = hostname_obj.getHostName()
    print(f'Current hostname is {current_name}')

    # Set hostname
    hostname_obj.setHostName(NEWNAME)
    new_name = hostname_obj.getHostName()
    print(f'Hostname change to {new_name}')

    select = input('Kill the hostName service y/n ?')
    if select.startswith('y'):
        hostname_obj.quit()


if __name__ == '__main__':
    main()
