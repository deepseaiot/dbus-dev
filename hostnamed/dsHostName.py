#!/usr/bin/env python3

"""
    Defines a Class that exports three services on DBus.
    In fact the only thing that differentiates this class
    definition from a normal one is the fact that the class
    docstring contains an XML object that discribes the
    behavior of the exported members.

    Exporetd members are:

        getHostName() -> str
        setHostName(str) -> bool
        quit()

    They are exported under the following interface:
        'deepsea.neuro.system.basic'

"""


# This is just for testing reasons.
TMP_HOSTNAME_FILE = "/tmp/.dstmphostname"


class HostNameServiceObj(object):
    """
        <node>
            <interface name='deepsea.neuro.system.basic'>
                <method name='getHostName'>
                    <arg type='s' name='response' direction='out'/>
                </method>
                <method name='setHostName'>
                    <arg type='s' name='new_name' direction='in'/>
                    <arg type='b' name='response' direction='out'/>
                </method>
                <method name='quit'/>
            </interface>
        </node>
    """
    def __init__(self, gloop):
        """ Ctor of the HostNameServiceObj that provides hostname service on DBus

            :param gloop:   A main event loop instance. It will be used
                            in order to exit DBus, when 'quit' method is
                            called.
            :type gloop:   gi.repository.GLib.MainLoop
        """
        self.__gloop = gloop
        with open(TMP_HOSTNAME_FILE, 'w') as fh:
            import os
            fh.write(os.uname()[1])

    def getHostName(self):
        """ Return the hostname of the machine. """
        print(" [DEBUG] CALLED: getHostName ")
        with open(TMP_HOSTNAME_FILE) as fh:
            name = fh.read().strip()
        return name

    def setHostName(self, new_name):
        """ Set a new hostname to the machine. """
        print(" [DEBUG] CALLED: setHostName ")
        with open(TMP_HOSTNAME_FILE, 'w') as fh:
            fh.write(new_name)
        return True

    def quit(self):
        """ quit docstring """
        print(" [DEBUG] CALLED: quit ")
        self.__gloop.quit()


# ################################################
#                   MAIN
# ################################################
def main():
    from gi.repository import GLib
    from pydbus import SessionBus
    from hostnamed.dsHostName import HostNameServiceObj

    busName = 'deepsea.neuro.system'
    export_objs = []

    # Create Event Loop
    loop = GLib.MainLoop()

    # Connect to bus
    bus = SessionBus()

    # Instansiate the object to be exported
    obj = HostNameServiceObj(loop)

    # Expose the object under the name  /deepsea/neuro/system/hostname
    export_objs.append(('hostname', obj))

    # Export all the objects
    # Note. pydbus allows only one call to publis()
    # It binds under the busname a number of objects.
    bus.publish(busName, *export_objs)

    # Enter Main loop
    loop.run()


if __name__ == '__main__':
    main()
