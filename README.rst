#########
hostnamed
#########

Description
-----------

A deamon process that connects to **system-DBus** and exports a service that allows other processes to change the hostname of the machine by sending a **Dbus messages** on the bus.


Dependencies
------------

+ `pydbus  <https://github.com/LEW21/pydbus>`_  : A new python3 DBus binding.

+ To provide an object on DBus a process must join an event loop. This creates a dependency to GLib. This is a system-distribution library and is not provided from the **PyPi**. Thus it must be installed manually, or the python package shall be replaced with at *.deb* package. ( `so-question <https://askubuntu.com/questions/80448/what-would-cause-the-gi-module-to-be-missing-from-python>`_ )

  System Wise:

  .. code:: bash

    $sudo apt install python3-gi

  When using virtual environments:

  .. code:: bash

    $pip install xenv
    $pip install xenv.pi

  .. note::

    It is observed that sometimes the installation of `xenv, xenv.pi` does not work due to the pip cache. Deletion of that directory seems to fix the issue.

    .. code:: bash

      $rm -r ~/.cache/pip

Usage/Test
----------

If package is successfully installed then, open two terminals and give the following commands.

- Terminal-A (act as server)

  .. code:: bash

    $ds-start-hostname

- Terminal-B (act as client)

  .. code:: bash

    $ds-test-hostname
